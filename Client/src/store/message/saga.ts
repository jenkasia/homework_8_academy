import { takeEvery, put, call } from 'redux-saga/effects';
import { GET_MESSAGES } from '../types';
import { showLoader, hideLoader } from '../chatInfo/actions';
import { putMessages } from './actions';

export function* watchLoadMessages() {
  yield takeEvery(GET_MESSAGES, workerLoadMessage);
}

function* workerLoadMessage() {
  yield put(showLoader());
  const payload = yield call(fetchMessages);
  yield put(putMessages(payload));
  yield put(hideLoader());
}

const fetchMessages = async () => {
  const response = await fetch('http://localhost:3050/api/messages/');
  return await response.json();
};
