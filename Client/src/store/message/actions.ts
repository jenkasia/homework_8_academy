import {
  ADD_MESSAGE,
  REMOVE_MESSAGE,
  LIKE_MESSAGE,
  EDIT_MESSAGE,
  GET_MESSAGES,
  HIDE_LOADER,
  PUT_MESSAGES
} from '../types';
import IMessage from '../../Interfaces/IMessage';

export const addMessage = (message: IMessage) => {
  return {
    type: ADD_MESSAGE,
    payload: message
  };
};

export const removeMessage = (messageID: string) => {
  return {
    type: REMOVE_MESSAGE,
    payload: messageID
  };
};

export const editMessage = (messageID: string, newMessage: string) => {
  return {
    type: EDIT_MESSAGE,
    payload: { messageID, newMessage }
  };
};

export const likeDislikeMessage = (messageID: string) => {
  return {
    type: LIKE_MESSAGE,
    payload: messageID
  };
};

export const fetchMessages = () => {
  return {
    type: GET_MESSAGES
  };
};

export const putMessages = (messages: IMessage) => {
  return {
    type: PUT_MESSAGES,
    payload: messages
  };
};
