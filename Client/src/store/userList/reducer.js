
import { FETCH_USERS_SUCCESS } from "../types";

export const usersReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_USERS_SUCCESS: {
      return [...action.payload.users];
    }

    default:
      return state;
  }
}
