import { GET_USER, PUT_USER, PUT_USER_FAIL, LOGOUT_USER } from '../types';

export const getUser = (user: any) => {
  return {
    type: GET_USER,
    payload: user
  };
};

export const putUserSuccess = (user: any) => {
  return {
    type: PUT_USER,
    payload: user
  };
};

export const putUserFail = (user: any) => {
  return {
    type: PUT_USER_FAIL,
    payload: user
  };
};

export const logoutUser = () => {
  return {
    type: LOGOUT_USER
  };
};
