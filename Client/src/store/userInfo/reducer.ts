import { PUT_USER_FAIL, PUT_USER, LOGOUT_USER } from '../types';

type Action = {
  type: string;
  payload: any;
};

const initialState = { user: null, error: '' };

export const userReducer = (state = initialState, action: Action) => {
  console.log('userReducer -> action', action);
  switch (action.type) {
    case PUT_USER: {
      return { ...state, user: action.payload, error: '' };
    }
    case PUT_USER_FAIL: {
      return { ...state, user: null, error: action.payload.message };
    }

    case LOGOUT_USER: {
      return { ...state, user: {} };
    }

    default: {
      return { ...state };
    }
  }
};
