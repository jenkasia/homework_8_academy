import { takeEvery, put, call } from 'redux-saga/effects';
import { GET_USER } from '../types';
import { showLoader, hideLoader } from '../chatInfo/actions';
import { putUserSuccess, putUserFail } from './action';

export function* watchLoadUser() {
  yield takeEvery(GET_USER, workerLoadUser);
}

function* workerLoadUser(user: any) {
  yield put(showLoader());
  try {
    console.log('dsd');
    const payload = yield call(fetchUser, user);
    yield put(hideLoader());
    console.log('function*workerLoadUser -> payload', payload);
    if (payload.error) {
      throw new Error(payload.message);
    }
    yield put(putUserSuccess(payload));
  } catch (error) {
    console.log(error);
    yield put(putUserFail(error));
  }
}

const fetchUser = async (user: any) => {
  const response = await fetch('http://localhost:3050/api/users/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(user.payload)
  });
  return await response.json();
};
