import { combineReducers } from 'redux';
import { appReducer } from './chatInfo/reducer';
import { messageReducer } from './message/reducer';
import { userReducer } from './userInfo/reducer';
import { usersReducer } from './userList/reducer';

export default combineReducers({
  app: appReducer,
  messages: messageReducer,
  user: userReducer,
  users: usersReducer
});
