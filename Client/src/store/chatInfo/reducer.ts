import { SHOW_LOADER, HIDE_LOADER } from '../types';

type Action = {
  type: string;
};

const initialState = {
  loading: true,
  chatName: 'Binary Studio Chat',
  currentUserId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4'
};

export const appReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    default:
      return state;
  }
};
