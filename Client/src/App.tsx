import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Footer from './components/Footer';
// import Header from './components/Header';
import Chat from './components/Chat';
import LoginPage from './components/LoginPage/LoginPage';
import UserList from './components/UserList/UserList';
import UserEditor from './components/UserEditor/UserEditor';
import MessageEditor from './components/MessageEditor/MessageEditor';
import AdminProtectedRoute from './components/ProtectedRoute/AdminProtectedRoute';
import UserProtectedRoute from './components/ProtectedRoute/UserProtectedRoute';
import './App.sass';
// import { editMessage } from './store/message/actions';

const App: React.FC = () => {
  return (
    <>
      {/* <Header /> */}
      {/* <Chat /> */}

      <Router>
        <Switch>
          <AdminProtectedRoute path='/userList' component={UserList} />
          <UserProtectedRoute path='/editMessage/:id' component={MessageEditor} />
          <AdminProtectedRoute path='/editUser/:user' component={UserEditor} />
          <UserProtectedRoute path='/chat' component={Chat} />
          <Route path='/' component={LoginPage} />
        </Switch>
        <Footer />
      </Router>
    </>
  );
};

export default App;
