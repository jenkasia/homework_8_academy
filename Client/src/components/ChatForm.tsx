import React, { useRef, FormEvent } from 'react';

interface ChatFormProps {
  onAdd(message: string): void;
}

const ChatForm: React.FC<ChatFormProps> = (props) => {
  const ref = useRef<HTMLTextAreaElement>(null);

  const onSubmitHandlerToSend = (event: React.FormEvent) => {
    event.preventDefault();
    const textMessage = ref.current!.value.trim();
    if (textMessage) {
      props.onAdd!(ref.current!.value);
      ref.current!.value = '';
    }
  };

  return (
    <form className='chat__form' onSubmit={onSubmitHandlerToSend}>
      <textarea className='form_input' id='story' name='story' ref={ref}></textarea>
      <button type='submit' className='form_button'>
        Send
      </button>
    </form>
  );
};

export default ChatForm;
