import React from 'react';
import { connect } from 'react-redux';
import Wrapper from './Wrapper';
import ChatHeader from './ChatHeader';
import ChatBody from './ChatBody';
import ChatForm from './ChatForm';
import LoaderSpinner from './LoaderSpinner';
import IMessage from '../Interfaces/IMessage';
import { addMessage, removeMessage, likeDislikeMessage, editMessage, fetchMessages } from '../store/message/actions';

interface IProps {
  loader: boolean;
  chatName: string;
  currentUserId: string;
  messages: Array<IMessage>;
  likedMessages: Array<string>;
  addMessage: Function;
  removeMessage: Function;
  likeDislikeMessage: Function;
  editMessage: Function;
  fetchMessages: Function;
}

class Chat extends React.Component<IProps> {
  componentDidMount() {
    this.props.fetchMessages();
  }

  getParticipantsCount = (messages: Array<IMessage>): number => {
    const participantsRepeating = messages.map((message) => message.userId);
    return new Set(participantsRepeating).size;
  };

  getLastMessageDate = (messages: Array<IMessage>) => {
    if (messages.length === 0) {
      return '';
    }
    return messages.sort((b, a) => {
      return Date.parse(a.createdAt) - Date.parse(b.createdAt);
    })[0].createdAt;
  };

  idGenerator = () => {
    const id = `f${(+new Date()).toString(16)}`;
    return id;
  };

  addMessageHandler = (message: string) => {
    const newMessage: IMessage = {
      id: this.idGenerator(),
      text: message,
      user: '',
      avatar: '',
      userId: this.props.currentUserId,
      editedAt: '',
      createdAt: new Date().toString()
    };
    this.props.addMessage(newMessage);
  };

  editMessageHandler = (messageID: string, textMessage: string) => {
    let newMessage = window.prompt('Измените своё сообщение', textMessage);
    if (newMessage && newMessage.trim) {
      this.props.editMessage(messageID, newMessage);
    } else {
      this.props.editMessage(messageID, textMessage);
    }
  };

  removeMessageHandler = (messageID: string) => {
    this.props.removeMessage(messageID);
  };

  toggleLike = (messageID: string) => {
    this.props.likeDislikeMessage(messageID);
  };

  render() {
    return this.props.loader ? (
      <LoaderSpinner></LoaderSpinner>
    ) : (
      <Wrapper class='chat'>
        <ChatHeader
          participants={this.getParticipantsCount(this.props.messages)}
          messagesAmount={this.props.messages.length}
          lastMessageDate={this.getLastMessageDate(this.props.messages)}
          chatName={this.props.chatName}
        />
        <ChatBody
          messages={this.props.messages}
          currentUserId={this.props.currentUserId}
          onRemoveMessage={this.removeMessageHandler}
          onLikeDislikeMessage={this.toggleLike}
          onEditMessage={this.editMessageHandler}
          likedMessagesList={this.props.likedMessages}
        />
        <ChatForm onAdd={this.addMessageHandler}></ChatForm>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    loader: state.app.loading,
    chatName: state.app.chatName,
    currentUserId: state.app.currentUserId,
    messages: state.messages.messages,
    likedMessages: state.messages.likedMessages
  };
};

const mapDispatchToProps = {
  addMessage,
  removeMessage,
  likeDislikeMessage,
  editMessage,
  fetchMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
