import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const AdminProtectedRoute = ({ component: Component, ...rest }) => {
  console.log(rest)
  return (
    <Route
      {...rest}
      render={(props) => {

        if (rest.user && (rest.user.type === 'admin')) {
          return <Component {...rest} {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: '/',
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user.user,
  };
};

// const mapDispatchToProps = {
//   addMessage,
//   removeMessage,
//   likeDislikeMessage,
//   editMessage,
//   fetchMessages
// };

export default connect(mapStateToProps)(AdminProtectedRoute);
