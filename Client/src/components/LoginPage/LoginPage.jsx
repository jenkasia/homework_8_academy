import React, { useRef } from 'react';
import { connect } from 'react-redux';
import Header from '../Header';
import Wrapper from '../Wrapper';
import { getUser } from '../../store/userInfo/action';
import './LoginPage.sass';
import { Redirect } from 'react-router-dom';

const LoginPage = (props) => {
  console.log(props);
  const emailInputRef = useRef(null);
  const passwordInputRef = useRef(null);
  const loginToApp = (e) => {
    e.preventDefault();
    props.dispatch(getUser({ username: emailInputRef.current.value, password: passwordInputRef.current.value }));
  };

  if (props.userInfo.user && props.userInfo.user.type === 'user') {
    return <Redirect to='/chat'></Redirect>;
  } else if (props.userInfo.user && props.userInfo.user.type === 'admin') {
    return <Redirect to='/userList'></Redirect>;
  }
  return (
    <>
      <Header />
      <Wrapper>
        <form className='loginForm' onSubmit={loginToApp}>
          <label htmlFor='email' className='loginLabel'>
            Email
          </label>
          <input id='email' type='text' className='loginInput' ref={emailInputRef} />
          <label htmlFor='password' className='loginLabel'>
            Password
          </label>
          <input id='password' type='password' className='loginInput' ref={passwordInputRef} />
          <button type='submit' className='submitButton'>
            Login
          </button>
        </form>
      </Wrapper>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    userInfo: state.user
  };
};

export default connect(mapStateToProps)(LoginPage);
