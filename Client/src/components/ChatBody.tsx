import React, { useEffect, useRef } from 'react';
import Message from './Message';
import IMessage from '../Interfaces/IMessage';

type ChatProps = {
  messages: IMessage[];
  currentUserId: string;
  likedMessagesList: Array<string>;
  onRemoveMessage(messageID: string): void;
  onLikeDislikeMessage(messageID: string): void;
  onEditMessage(messageID: string, messageText: string): void;
};

const ChatBody: React.FC<ChatProps> = (props) => {
  if (props.messages.length === 0) {
    return <div></div>;
  }

  const checkIsCurrentUser = (userID: string, currentMessageUserId: string): boolean => {
    return userID === currentMessageUserId;
  };

  const parseDate = (dateString: string): string => {
    let date = new Date(dateString);
    let time = `${date.getHours()}:${date.getMinutes()}`;
    return time;
  };

  // const messagesEndRef = useRef<HTMLDivElement>(null);

  // const scrollToBottom = () => {
  //   messagesEndRef.current!.scrollIntoView!({ behavior: 'smooth' });
  // };

  // useEffect(scrollToBottom, [props.messages]);

  const yourMessagesArray = props.messages
    .filter((message) => message.userId === props.currentUserId)
    .sort((a, b) => Date.parse(a.createdAt) - Date.parse(b.createdAt));
  let yourLastMessageId = yourMessagesArray[yourMessagesArray.length - 1].id;

  return (
    <div className='chat__body'>
      {props.messages
        .sort((b, a) => {
          return Date.parse(a.createdAt) - Date.parse(b.createdAt);
        })
        .map((message) => {
          return (
            <Message
              key={message.id}
              messageType={checkIsCurrentUser(props.currentUserId, message.userId) ? 'personal' : ''}
              messageID={message.id}
              messageText={message.text}
              imageSrc={message.avatar}
              userName={message.user}
              onRemoveMessage={props.onRemoveMessage}
              onEditMessage={props.onEditMessage}
              onLikeDislikeMessage={props.onLikeDislikeMessage}
              messageData={parseDate(message.createdAt)}
              likedMessagesList={props.likedMessagesList}
              editableMessage={message.id === yourLastMessageId}
            ></Message>
          );
        })
        .reverse()}
      <div />
    </div>
  );
};

export default ChatBody;
