const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req, res, next) {
  res.body = UserService.searchAll()
  res.error = `There are no users on server`
  next()
}, responseMiddleware)


// router.get('/:id', function (req, res, next) {
//   res.body = UserService.search(req.params.id)
//   res.error = `User with id:${req.params.id} not found`
//   next()
// }, responseMiddleware)


router.post('/', function (req, res, next) {
  console.log(req.body)
  res.body = UserService.authCheck(req.body)
  console.log("res.body", res.body)
  res.error = `Incorrect login or password`
  next()
}, responseMiddleware)


// router.put('/:id', updateUserValid, function (req, res, next) {
//   res.body = UserService.updateUser(req.params.id, req.body)
//   res.error = `User is not found`
//   next()
// }, responseMiddleware)


// router.delete('/:id', function (req, res, next) {
//   res.body = UserService.deleteUser(req.params.id)
//   res.error = `User is not found`
//   next()
// }, responseMiddleware)

module.exports = router;