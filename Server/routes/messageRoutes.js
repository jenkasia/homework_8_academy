const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// OPTIONAL TODO: Implement route controller for messages
router.get('/', function (req, res, next) {
  res.body = MessageService.searchAll()
  res.error = `There are no messages on server`
  next()
}, responseMiddleware)

module.exports = router;