const express = require('express')
const cors = require('cors');
const { morganMiddleware } = require('./config/morgan');

require('./config/db');

const app = express()

app.use(cors());
app.use(morganMiddleware)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

app.use('*', function (req, res) {
  res.status(404).send('Server don`t know anything about this request');
});

const port = 3050;
app.listen(port, () => { });

exports.app = app;