const { MessageRepository } = require('../repositories/messageRepository');

class MessagesService {
    // OPTIONAL TODO: Implement methods to work with messages
    searchAll() {
        const item = MessageRepository.getAll();
        if (item.length == 0) {
            return null;
        }
        return item;
    }
}

module.exports = new MessagesService();