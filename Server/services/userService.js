const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    searchAll() {
        const item = UserRepository.getAll();
        if (item.length == 0) {
            return null;
        }
        return item;
    }

    search(userId) {
        const item = UserRepository.getOne({ id: userId });
        if (!item) {
            return null;
        }
        return item;
    }

    authCheck(user) {
        const item = UserRepository.getOne({ userName: user.username });
        if (!item || item.password !== user.password) {
            return null;
        }
        return item
    }


    createUsers(data) {
        const existingChecker = !!(UserRepository.getOne({ phoneNumber: data.phoneNumber }) || UserRepository.getOne({ email: data.email }))
        if (existingChecker) {
            return null
        }
        else {
            const result = UserRepository.create(data)
            return result
        }
    }

    updateUser(userId, data) {
        const itemAvailibility = !!this.search(userId)

        if (itemAvailibility) {
            const item = UserRepository.update(userId, data);
            return item;
        }
        else {
            return null;
        }
    }

    deleteUser(userId) {
        const item = UserRepository.delete(userId);
        if (!item) {
            return null;
        }
        return item[0];
    }
}

module.exports = new UserService();