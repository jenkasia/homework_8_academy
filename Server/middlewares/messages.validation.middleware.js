const { message } = require('../models/message');

const createMessageValid = (req, res, next) => {
    // TODO: Implement validation for message entity during creation
    next();
}

const updateMessageValid = (req, res, next) => {
    // TODO: Implement validation for message entity during update
    next();
}

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;